import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.utils.data import sampler

import torchvision.datasets as dset
import torchvision.transforms as T

import numpy as np
import matplotlib.pyplot as plt
import timeit

class ChunkSampler(sampler.Sampler):
    """Samples elements sequentially from some offset.
    Arguments:
        num_samples: # of desired datapoints
        start: offset where we should start selecting from
    """
    def __init__(self, num_samples, start = 0):
        self.num_samples = num_samples
        self.start = start

    def __iter__(self):
        return iter(range(self.start, self.start + self.num_samples))

    def __len__(self):
        return self.num_samples

NUM_TRAIN = 49000
NUM_VAL = 1000
BATCH_SIZE = 64
cifar10_train = dset.CIFAR10('./cs231n/datasets', train=True, download=True,
                           transform=T.ToTensor())
loader_train = DataLoader(cifar10_train, batch_size=BATCH_SIZE, sampler=ChunkSampler(NUM_TRAIN, 0))

cifar10_val = dset.CIFAR10('./cs231n/datasets', train=True, download=True,
                           transform=T.ToTensor())
loader_val = DataLoader(cifar10_val, batch_size=BATCH_SIZE, sampler=ChunkSampler(NUM_VAL, NUM_TRAIN))

cifar10_test = dset.CIFAR10('./cs231n/datasets', train=False, download=True,
                          transform=T.ToTensor())
loader_test = DataLoader(cifar10_test, batch_size=BATCH_SIZE)

class Flatten(nn.Module):
    def forward(self, x):
        N, C, H, W = x.size() # read in N, C, H, W
        return x.view(N, -1)  # "flatten" the C * H * W values into a single vector per image

def train(model, loss_fn, optimizer, num_epochs = 1, print_every = 500):
 
    for epoch in range(num_epochs):
        print('Starting epoch %d / %d' % (epoch + 1, num_epochs))
        model.train()
        #g_loss= []
        for t, (x, y) in enumerate(loader_train):
            x_var = Variable(x.type(gpu_dtype))
            y_var = Variable(y.type(gpu_dtype).long())
            optimizer.zero_grad()
            scores = model(x_var)
            print y_var
            break
            loss = loss_fn(scores, y_var)
            #g_loss.append(loss.data[0])
            if (t + 1) % print_every == 0:
                print('t = %d, loss = %.4f' % (t + 1, loss.data[0]))
            loss.backward()
            optimizer.step()
        #fig, ax = plt.subplots(nrows=1, ncols=1)
        #ax.plot(g_loss)
        #ax.set_title('Loss at {} epoch'.format(epoch))
        #fig.savefig('{}.png'.format(epoch))
        #plt.close(fig)

def check_accuracy(model, loader):
    if loader.dataset.train:
        print('Checking accuracy on validation set')
    else:
        print('Checking accuracy on test set')
    num_correct = 0
    num_samples = 0
    model.eval() # Put the model in test mode (the opposite of model.train(), essentially)
    for x, y in loader:
        x_var = Variable(x.type(gpu_dtype), volatile=True)
        y = Variable(y.type(torch.cuda.LongTensor))
        scores = model(x_var)
        _, preds = torch.max(scores.data, 1)
        num_correct += (preds == y.data).cpu().sum()
        num_samples += preds.size(0)
    acc = float(num_correct) / num_samples
    print('Got %d / %d correct (%.2f)' % (num_correct, num_samples, 100 * acc))

def reset(m):
    if hasattr(m, 'reset_parameters'):
        m.reset_parameters()

def init_weights(m):
    if type(m) == nn.Conv2d:
        n = m.kernel_size[0]*m.kernel_size[1]*m.out_channels
        m.weight.data.normal_(0, np.sqrt(2.0/n))
    if type(m) == nn.BatchNorm2d:
        m.weight.data.fill_(1)
        m.bias.data.zero_()

gpu_dtype = torch.cuda.FloatTensor

LeNet5model = nn.Sequential(
                    nn.Conv2d(3, 6, kernel_size=5, stride=1, padding=0),
                    nn.ReLU(inplace=True),
                    nn.BatchNorm2d(6),
                    nn.MaxPool2d((2,2), stride=2),
                    nn.Conv2d(6, 16, kernel_size=5, stride=1, padding=0),
                    nn.ReLU(inplace=True),
                    nn.BatchNorm2d(16),
                    nn.MaxPool2d((2,2), stride=2),
                    Flatten(),
                    nn.Linear(16*5*5, 120),
                    nn.ReLU(inplace=True),
                    nn.Linear(120,84),
                    nn.ReLU(inplace=True),
                    nn.Linear(84,10),
                    #nn.Softmax()
            )

#LeNet5model = LeNet5model.type(gpu_dtype)
#loss_fn = nn.CrossEntropyLoss().type(gpu_dtype)
#RMSoptimizer = optim.RMSprop(LeNet5model.parameters(), lr=1e-3, weight_decay=1e-3)
#SGDoptimizer = optim.SGD(LeNet5model.parameters(), lr=1e-3, momentum=0.9)
#Adamoptimizer = optim.Adam(LeNet5model.parameters(), lr=1e-3, weight_decay=1e-3)
#
#torch.cuda.synchronize()
#train(LeNet5model, loss_fn, RMSoptimizer, num_epochs=20)
#check_accuracy(LeNet5model, loader_val)
#LeNet5model.apply(reset)
#torch.cuda.synchronize()
#train(LeNet5model, loss_fn, SGDoptimizer, num_epochs=20)
#check_accuracy(LeNet5model, loader_val)
#LeNet5model.apply(reset)
#torch.cuda.synchronize()
#train(LeNet5model, loss_fn, Adamoptimizer, num_epochs=20)
#check_accuracy(LeNet5model, loader_val)

MobNet = nn.Sequential(
        #in 3*32*32
        nn.Conv2d(3, 32, kernel_size=3, stride=2,padding=1),
        # 32*16*16
        nn.BatchNorm2d(32),
        nn.ReLU(True),
        nn.Conv2d(32,32, kernel_size=3, stride=1, padding=1),
        #32*16*16
        nn.BatchNorm2d(32),
        nn.ReLU(True),
        nn.Conv2d(32, 64, kernel_size=1, stride=1),
        #64*16*16
        nn.BatchNorm2d(64),
        nn.ReLU(True),
        #nn.Dropout(),
        nn.Conv2d(64,64, kernel_size=3, stride=2, padding=1),
        #64*8*8
        nn.BatchNorm2d(64),
        nn.ReLU(True),
        #Flatten(),
        nn.Conv2d(64,128, kernel_size=3, stride=1, padding=1),
        #128*8*8
        nn.BatchNorm2d(128),
        nn.ReLU(True),
        nn.Conv2d(128,256,kernel_size=1, stride=1),
        #256*8*8
        nn.BatchNorm2d(256),
        nn.ReLU(True),
        #nn.Dropout(),
        nn.AvgPool2d((8,8), stride=1),
        Flatten(),
        nn.Linear(256,128),
        nn.ReLU(True),
        nn.Dropout(),
        nn.Linear(128, 64),
        nn.ReLU(True),
        nn.Dropout(),
        nn.Linear(64,10),
        )
#MobNet.apply(init_weights)
#MobNet = MobNet.type(gpu_dtype)
#loss_fn = nn.CrossEntropyLoss().type(gpu_dtype)
#SGDoptimizer = optim.SGD(MobNet.parameters(), lr=1e-3, momentum=0.9)
#RMSoptimizer = optim.RMSprop(MobNet.parameters(), lr=1e-3, weight_decay=1e-3)
#Adamoptimizer = optim.Adam(MobNet.parameters(), lr=1e-3, weight_decay=1e-3)
#AGoptimizer = optim.Adagrad(MobNet.parameters(),lr=1e-1, weight_decay=1e-3)
#torch.cuda.synchronize()
#train(MobNet, loss_fn, RMSoptimizer, num_epochs=20)
#check_accuracy(MobNet, loader_val)

model = nn.Sequential(
        nn.Conv2d( 3, 32, kernel_size=3, stride=1, padding=1),
        nn.ReLU(True),
        nn.BatchNorm2d(32),
        nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
        nn.ReLU(True),
        nn.BatchNorm2d(32),
        nn.MaxPool2d((2,2)),
        #32*16*16
        nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
        nn.ReLU(True),
        nn.BatchNorm2d(32),
        nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
        nn.ReLU(True),
        nn.BatchNorm2d(32),
        nn.MaxPool2d((2,2)),
        #32*8*8
        nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
        nn.ReLU(True),
        nn.BatchNorm2d(32),
        nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
        nn.ReLU(True),
        nn.BatchNorm2d(32),
        nn.MaxPool2d((2,2)),
        #32*4*4
        Flatten(),
        nn.Linear(32*4*4, 1024),
        nn.Dropout(0.65),
        nn.Linear(1024,256),
        nn.Dropout(0.65),
        nn.Linear(256,10)
        #nn.Softmax()
        )
model.apply(init_weights)
model = model.type(gpu_dtype)
loss_fn = nn.CrossEntropyLoss()
SGDoptimizer = optim.SGD(model.parameters(), lr=1e-3, momentum=0.9)
RMSoptimizer = optim.RMSprop(model.parameters(), lr=1e-3, weight_decay=1e-3)
Adamoptimizer = optim.Adam(model.parameters(), lr=1e-3, weight_decay=1e-3)
AGoptimizer = optim.Adagrad(model.parameters(),lr=1e-2, weight_decay=1e-2)
torch.cuda.synchronize()
train(model, loss_fn, Adamoptimizer, num_epochs=1, print_every=500)
#check_accuracy(model, loader_val)
#best set - kernel_sizes=7,5,3,stride=1,padding=3,2,1; Adam with  lr=1e-3, weight_decay=1e-3;batch size=50
#plt.show()

def plot_kernels(tensor):
    if not tensor.ndim==4:
        raise Exception("assumes a 4D tensor")
    if not tensor.shape[-1]==3:
        raise Exception("last dim needs to be 3 to plot")
    num_kernels = tensor.shape[0]
    num_rows = 1+ num_kernels // num_cols
    fig = plt.figure(figsize=(num_cols,num_rows))
    for i in range(tensor.shape[0]):
        ax1 = fig.add_subplot(num_rows,num_cols,i+1)
        ax1.imshow(tensor[i])
        ax1.axis('off')
        ax1.set_xticklabels([])
        ax1.set_yticklabels([])

    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.show()

#mm = model.double()
#filters = mm.modules
#body_model = [i for i in mm.children()][0]
#plot_kernels(body_model[0].weight.data.numpy())
